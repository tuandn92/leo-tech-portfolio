import React from 'react'

export default function Service() {
    return (
        <section>
            <div className="service">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="title">
                                <h2>Our Services</h2>
                                <p>We provide are outsourcing and production services for Web, Mobile, ERP and CMS Development to Small and medium-sized enterprises (SME).</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="service-card">
                                <div className="card-img">
                                    <a href=""><img src="/assets/images/homepage/web.jpg" alt="" /></a>
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">Web Development</h5>
                                    <p className="card-text">
                                        Web development is the work involved in developing a website for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web-based internet applications, electronic businesses, and social network services.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="service-card">
                                <div className="card-img">
                                    <a href=""><img src="/assets/images/homepage/mobile.jpg" alt="" /></a>
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">Mobile App Development</h5>
                                    <p className="card-text">
                                    Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="service-card">
                                <div className="card-img">
                                    <a href=""><img src="/assets/images/homepage/erp-cms.jpg" alt="" /></a>
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">ERP and CMS</h5>
                                    <p className="card-text">
                                       
                                    Enterprise resource planning is the integrated management of main business processes, often in real time and mediated by software and technology.
                                    
                                    A content management system is a software application that can be used to manage the creation and modification of digital content. CMSs are typically used for enterprise content management and web content management.
                                    
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
