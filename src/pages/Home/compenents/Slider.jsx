import React from 'react'

export default function Slider() {
    return (
        <section>
            <div className="slider">
                <div className="container">
                    <div className="row">
                        <div className="col-md-7">
                            <div className="content">
                                <h2>CEO & Founder</h2>
                                <div className="text">
                                    <p> My name is Leo (Hoang Tra). My life’s mission is to become a professional software engineer,
                                    and applying technology to life.
                                    </p>
                                    <p>
                                        #LoveBook, I have been following this quote "Doing what you like is freedom. Liking what you
                                        do is happy" as the purpose of my life!
                                    </p>
                                    <p>
                                        #LoveCode, I like to code, I love to build things with code and learn new things.
                                    </p>
                                    <p>
                                        #LoveCoffee, I love coffee, and I start every day with a morning cup of coffee.
                                    </p>
                                    <p>
                                        Finally, Which is my perfect day.
                                    </p>
                                    <p>
                                        TECHNOLOGIES USED: .NET, Python, NodeJS, PHP, Angular, React, AI & Machine Learning, SQL (MySQL, PostgreSQL, SQL Server) & NoSQL (MongoDB, Redis)
                                    </p>
                                </div>
                            </div>
                            <div className="follow">
                                <a href=""><img src="/assets/images/icon/facebook-logo.png" alt=""/></a>
                                <a href=""><img src="/assets/images/icon/twitter.png" alt=""/></a>
                                <a href=""><img src="/assets/images/icon/instagram.png" alt=""/></a>
                                <a href=""><img src="/assets/images/icon/linkedin.png" alt=""/></a>
                                {/* <a href=""><img src="/assets/images/icon/youtube.png" alt=""/></a> */}
                            </div>
                        </div>
                        <div className="col-md-5">
                            <div className="content-image">
                                <img src="/assets/images/leo-tech.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
