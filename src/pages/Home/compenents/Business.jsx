import React from 'react'

export default function Business() {
    return (
        <section>
            <div className="business">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="title">
                                <h2>For All Business Inquiries Contact</h2>
                                <p>
                                We are ready to help you build the information you need to start, run or grow your business.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="business-card">
                                <div className="card-img">
                                    <a href=""><img src="/assets/images/homepage/startup.jpg" alt=""/></a>
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">
                                    Startup
                                    </h5>
                                    <p className="card-text">
                                    Startup Business Ideas That Can Make You Money.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="business-card">
                                <div className="card-img">
                                    <a href=""><img src="/assets/images/homepage/business.jpg" alt=""/></a>
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">
                                    Business
                                    </h5>
                                    <p className="card-text">
                                    I’d love to chat with you about tech or potential business, shot me an email.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
