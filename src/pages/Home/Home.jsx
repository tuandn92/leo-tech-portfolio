import React from 'react'
import Business from './compenents/Business'
import Learning from './compenents/Learning'
import Service from './compenents/Service'
import Slider from './compenents/Slider'

export default function Home() {
    return (
        <div>
            <div className="homepage">
                <Slider />
                <Business />
                <Service />
                <Learning />
            </div>
        </div>
    )
}
