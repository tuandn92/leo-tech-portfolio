import React from 'react'
import TeamMembers from './components/TeamMembers'

export default function Team() {
    return (
        <main className="team">
            <div className="team-title">
                <h1>Embrace Families</h1>
            </div>
            <section>
                <div className="team-member">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-10 offset-lg-1">
                                <div className="row">
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/leo-tech.png"
                                        title="Hoang Tra"
                                        position="CEO & Founder"
                                         />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/ngan-nguyen-1.png"
                                        title="Ngan Nguyen"
                                        position="Lead Mobile"
                                         />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/tuan-nguyen.png"
                                        title="Tuan Nguyen"
                                        position="Tech Lead" />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/trang-nguyen-1024x1015.jpg"
                                        title="Trang Nguyen"
                                        position="Software Engineer Mobile" />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/toan-nguyen-1024x1024.jpg"
                                        title="Toan Nguyen"
                                        position="Software Engineer" />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/dat-nguyen-1-1024x1024.jpg"
                                        title="Dat Nguyen"
                                        position="Software Engineer" />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/tuan-tran.jpg"
                                        title="Tuan Tran"
                                        position="Software Engineer" />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers 
                                        cover="/assets/images/software-engineer.png"
                                        title="Loc Le"
                                        position="Software Engineer"/>
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                         cover="/assets/images/vu-le.jpg"
                                         title="Vu Le"
                                         position="Software Engineer" />
                                    </div>
                                    <div className="col-md-4">
                                        <TeamMembers
                                        cover="/assets/images/you.png"
                                        title=""
                                        position="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
}
