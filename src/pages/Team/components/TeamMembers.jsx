import React from 'react'

export default function TeamMembers({cover, title, position}) {
    return (
        <div className="members">
            <div className="members-img">
                <figure>
                    <img src={cover} alt="" />
                </figure>
            </div>
            <div className="members-infor">
                <div className="infor">
                    <h2 className="name">{title}</h2>
                    <p className="position">{position}</p>
                </div>
            </div>
        </div>
    )
}
