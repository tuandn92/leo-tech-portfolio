
import './styles/App.scss';
import 'bootstrap/dist/css/bootstrap.css'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from './components/Header';
import Home from './pages/Home/Home';
import Footer from './components/Footer';
import Team from './pages/Team/Team';
function App() {
  return (
    <Router>
      <Header />
      <Route path="/" exact component={Home} />
      <Route path="/team" component={Team}/>
      <Footer />
    </Router>
  );
}

export default App;
