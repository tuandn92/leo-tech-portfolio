import React from 'react'

export default function Footer() {
    return (
        <footer className="footer">
            <div className="content">
                <a href="">2021 © Leo Tech, </a>
                <a href="">proudly powered by Leo Tech Asia</a>
            </div>
        </footer>
    )
}
