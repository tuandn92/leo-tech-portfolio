import React from 'react'
import {Link} from 'react-router-dom'

export default function Header() {
    return (
        <header className="header">
            <div className="container">
                <div className="logo">
                    <a href=""><img src="/assets/images/cropped-logo.png" alt="logo" /></a>
                </div>
                <div className="nav">
                    <ul>
                        <li><Link to="/">home</Link></li>
                        <li><Link to="/team">team</Link></li>
                        <li><Link to="#">courses</Link></li>
                        <li><Link to="#">resources</Link></li>
                        <li><Link to="#">careers</Link></li>
                        <li><Link to="#">contact</Link></li>
                    </ul>
                </div>
            </div>
        </header>
    )
}
